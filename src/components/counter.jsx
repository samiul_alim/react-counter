import React, { Component } from "react";

class Counter extends Component {
  // state = {
  //   value: this.props.counter.value
  // };

  // constructor(){
  //   super();
  //   this.handleIncrement = this.handleIncrement.bind(this);
  // }

  // handleIncrement = () => {
  //   this.setState({ value: this.state.value + 1 });
  // };

  componentWillUnmount(){
    console.log('counter unmount');
  }

  render() {
    return (
      <div>
        <span className={this.getCounterClasses()}>{this.formatCount()}</span>

        <button
          onClick={() => this.props.onIncrement(this.props.counter)}
          className="btn btn-secondary btn-sm m-1"
        >
          Increment
        </button>

        <button
          onClick={() => this.props.onDelete(this.props.counter.id)}
          className="btn btn-danger btn-sm m-2"
        >
          Delete
        </button>
      </div>
    );
  }

  getCounterClasses() {
    let counterClasses = "badge badge-";
    counterClasses += this.props.counter.value > 0 ? "primary" : "warning";
    counterClasses += " m-2";
    return counterClasses;
  }

  formatCount() {
    const value = this.props.counter.value;
    return value === 0 ? "zero" : value;
  }
}

export default Counter;
